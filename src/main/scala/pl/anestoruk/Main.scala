package pl.anestoruk

import pl.anestoruk.engines.BashEngine
import pl.anestoruk.utils.ConfigSupport
import pl.anestoruk.utils.Utils._
import scala.util.Try

object Main extends ConfigSupport {

  val DEFAULT_PAGES = 10

  val DEFAULT_OUTPUT = "./output/result.json"

  def main(args: Array[String]): Unit = {

    // read `pages` from `args` or use default value if nothing is provided
    val pages = (if (!args.isEmpty) Try(args.head.toInt).toOption else None)
      .getOrElse(DEFAULT_PAGES)

    // read output file path from application.conf or use default value
    val file = Try(settingsConfig.getString("outputFile"))
      .getOrElse(DEFAULT_OUTPUT)

    val engine = BashEngine(file)

    // crawl and save collected data
    val (result, time) = execTime {
      () => {
        val data = engine.crawl(pages)
        engine.save(data).recover { case e => println(e.getMessage) }
        data
      }
    }

    // print basic stats
    println(s"\nTotal number of Posts found: ${result.size}")
    println(s"Average time to get 1 page: ${time / pages.toFloat} ms")
    println(s"Average time to get 1 post: ${time / result.size.toFloat} ms")
    println(s"Total execution time: ${time / 1000.toFloat} seconds")
    println(s"Results stored in ${engine.path.toRealPath()}\n")
  }

}