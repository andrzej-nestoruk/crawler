package pl.anestoruk.utils

import com.typesafe.config.ConfigFactory

/**
 * Mix in this trait for simple access to config settings
 */
trait ConfigSupport {

  val config = ConfigFactory.load()
  
  val settingsConfig = config.getConfig("settings")

}