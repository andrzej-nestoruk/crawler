package pl.anestoruk.utils

object Utils {

  /**
   * Executes `f` function and returns it's execution time
   *
   * @param f function to execute
   * @tparam T type returned by `f`
   */
  def execTime[T](f: () => T): (T, Long) = {
    val start = System.currentTimeMillis
    val result = f()
    val end = System.currentTimeMillis
    (result, end - start)
  }

}
