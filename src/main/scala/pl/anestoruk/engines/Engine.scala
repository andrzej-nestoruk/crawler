package pl.anestoruk.engines

import pl.anestoruk.models.Entity
import scala.util.Try

/**
 * Basic crawling-engine trait
 *
 * @tparam E Entity type
 */
trait Engine[E <: Entity] {

  val baseUrl: String

  def crawl(pages: Int): Seq[E]

  def save(data: Seq[E]): Try[Unit]

}