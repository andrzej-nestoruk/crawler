package pl.anestoruk.engines

import java.nio.file.StandardOpenOption.APPEND
import java.nio.file.{Files, Path, Paths}
import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import pl.anestoruk.models.Bash._
import pl.anestoruk.utils.Utils._
import play.api.libs.json.Json
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Success, Try}

/**
 * Crawling Engine for bash.org.pl
 *
 * @param browser ScalaScrapper's browser
 * @param file output for saving result
 * @param verbose when true, additional messages will be printed during crawling process
 */
case class BashEngine(
    file: String,
    browser: Browser = JsoupBrowser(),
    verbose: Boolean = true) extends Engine[Post] {

  val baseUrl: String = "http://bash.org.pl/latest/?page="
  val path: Path = Paths.get(file)

  /**
   * Searches HTMLs returned from specified URLs for bash.org.pl's Posts
   *
   * @param pages number of pages to crawl
   * @return collected Posts
   */
  def crawl(pages: Int): Seq[Post] = collectResult {
    for {
      page <- 1 to pages
      url = s"$baseUrl$page"
    } yield Future {
      val (result, time) = execTime { () => Try(browser.get(url)) }

      if (verbose) {
        val message = if (result.isSuccess) s"GET $url request: Success! took: $time ms"
        else s"GET $url request: Failure (404)! took: $time ms"
        println(message)
      }

      result.map { doc =>
        (doc >> elementList(".post")).map { item =>
          Post(extractId(item), extractPoints(item), extractContent(item))
        }
      }
    }
  }

  /**
   * Saves sequence of Posts to the output file in JSON format.
   */
  def save(data: Seq[Post]): Try[Unit] = {
    // recreate output file if it already exists
    Files.deleteIfExists(path)
    Files.createDirectories(path.getParent)
    Files.createFile(path)

    // transform sequence of Posts into Json array
    val json = Json.prettyPrint(Json.toJson(data))

    val writer = Files.newBufferedWriter(path, APPEND)
    val result = Try(writer.write(json, 0, json.length))
    writer.close()

    result
  }

  /**
   * Collects results from sequence of Futures into flat sequence of Posts and returns it.
   */
  private def collectResult(data: Seq[Future[Try[Seq[Post]]]]): Seq[Post] = {
    val future = Future
      .sequence(data)
      .map(_.collect { case Success(s) => s }.flatten)

    Await.result(future, 60.seconds)
  }

}
