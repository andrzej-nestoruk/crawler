package pl.anestoruk

import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model._
import org.apache.commons.lang3.StringEscapeUtils._
import play.api.libs.json.{Json, OFormat}

package object models {

  type ID = Long

  trait Entity {
    val id: ID
  }

  /**
   * Bash 'domain' models and types
   */
  object Bash {

    type Points = Long

    type Content = String

    case class Post(
        id: ID,
        points: Points,
        content: Content) extends Entity

    /**
     * Play Json formatter which allows to transform Post into JSON
     */
    implicit val postFormat: OFormat[Post] = Json.format[Post]

    def extractId(el: Element): ID = el.attr("id").tail.toLong

    def extractPoints(el: Element): Points = (el >> element(".points")).innerHtml.toLong

    def extractContent(el: Element): Content =
      unescapeHtml4((el >> element(".post-content")).innerHtml.replaceAll("""[\s]?\n""", "\n"))

  }

}