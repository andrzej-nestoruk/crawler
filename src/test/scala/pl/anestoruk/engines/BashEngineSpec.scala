package pl.anestoruk.engines

import java.nio.file.{Files, Paths}
import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.browser.JsoupBrowser.JsoupDocument
import org.jsoup.Jsoup
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import pl.anestoruk.models.Bash._
import play.api.libs.json.{JsSuccess, Json}
import scala.collection.JavaConverters._

class BashEngineSpec extends WordSpec with Matchers with MockFactory with BeforeAndAfterAll {

  val filename = "./output/test.json"

  // contains 3 posts
  val html1 ="""
      <div id="d4862636" class="q post"> <div class="bar"> <div class="right"> 28 marca 2017 20:02 </div> <a class="qid click" href="/4862636/">#4862636</a> <a class="click votes rox" rel="nofollow" href="/rox/4862636/">+</a> <span class=" points">-94</span> <a class="click votes sux" rel="nofollow" href="/sux/4862636/">-</a> <span class="msg">&nbsp;</span> </div> <div class="quote post-content post-body"> POST1 </div> </div>
      <div id="d4862635" class="q post"> <div class="bar"> <div class="right"> 28 marca 2017 12:02 </div> <a class="qid click" href="/4862635/">#4862635</a> <a class="click votes rox" rel="nofollow" href="/rox/4862635/">+</a> <span class=" points">407</span> <a class="click votes sux" rel="nofollow" href="/sux/4862635/">-</a> <span class="msg">&nbsp;</span> </div> <div class="quote post-content post-body"> POST2 </div> </div>
      <div id="d4862638" class="q post"> <div class="bar"> <div class="right"> 28 marca 2017 08:02 </div> <a class="qid click" href="/4862638/">#4862638</a> <a class="click votes rox" rel="nofollow" href="/rox/4862638/">+</a> <span class=" points">-193</span> <a class="click votes sux" rel="nofollow" href="/sux/4862638/">-</a> <span class="msg">&nbsp;</span> </div> <div class="quote post-content post-body"> POST3 </div>"""
  val doc1 = JsoupDocument(Jsoup.parse(html1))

  // contains 2 posts
  val html2 = """
      <div id="d4862546" class="q post"> <div class="bar"> <div class="right"> 27 marca 2017 20:02 </div> <a class="qid click" href="/4862546/">#4862546</a> <a class="click votes rox" rel="nofollow" href="/rox/4862546/">+</a> <span class=" points">495</span> <a class="click votes sux" rel="nofollow" href="/sux/4862546/">-</a> <span class="msg">&nbsp;</span> </div> <div class="quote post-content post-body"> POST4 </div> </div>
      <div id="d4862526" class="q post"> <div class="bar"> <div class="right"> 27 marca 2017 14:02 </div> <a class="qid click" href="/4862526/">#4862526</a> <a class="click votes rox" rel="nofollow" href="/rox/4862526/">+</a> <span class=" points">345</span> <a class="click votes sux" rel="nofollow" href="/sux/4862526/">-</a> <span class="msg">&nbsp;</span> </div> <div class="quote post-content post-body"> POST5 </div> </div>"""
  val doc2 = JsoupDocument(Jsoup.parse(html2))

  trait Context {
    val browserMock = mock[Browser]
    (browserMock.get _).expects("http://bash.org.pl/latest/?page=1").returning(doc1)
    (browserMock.get _).expects("http://bash.org.pl/latest/?page=2").returning(doc2)
    val engine = BashEngine(filename, browserMock, verbose = false)
  }

  override def afterAll = {
    Files.deleteIfExists(Paths.get(filename))
  }

  "crawl method" should {
    "return all 5 Posts from provided HTML pages" in new Context {
      val result = engine.crawl(3)
      result.size shouldBe 5
    }
  }

  "save method" should {
    "create output file" in new Context {
      val data = engine.crawl(3)
      engine.save(data)
      Files.exists(engine.path) shouldBe true
    }
  }

  "save method" should {
    "create output file with all collected Posts in JSON format" in new Context {
      val data = engine.crawl(3)
      val result = engine.save(data)

      val json = Json.parse(Files.readAllLines(engine.path).asScala.mkString)
      val expected = Seq(
        Post(4862636, -94, "POST1"),
        Post(4862635, 407, "POST2"),
        Post(4862638, -193, "POST3"),
        Post(4862546, 495, "POST4"),
        Post(4862526, 345, "POST5"))

      json.validate[Seq[Post]] shouldBe JsSuccess(expected)
    }
  }

}
