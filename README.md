# CRAWLER

## Starting
Running application:

```
sbt run
```

Application can be run with additional `pages` parameter (default is __10__) which allows to specify how many __bash.org.pl__'s pages should be parsed during crawling process.

for example:

```
sbt run 100
```
will crawl pages from 1 to 100

---

To run tests execute:
```
sbt test
```

## Output

Collected results are stored within output file specified in `application.conf`

partial, example output:

```
[ {
  "id" : 4862526,
  "points" : 344,
  "content" : "<Bambo> lubię siedzieć sobie po nocach (...)
}, {
  "id" : 4862438,
  "points" : 496,
  "content" : "<rd> \"lodówka z inteligentnymi drzwiczkami, wyświetlającymi zawartość wnętrza\" (...)
}, 

(...)

]
```